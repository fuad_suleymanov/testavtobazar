package test.avtobazar;

import sun.rmi.server.Dispatcher;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(name = "HelloServlet" , value = "/salam")
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        //printWriter.println("Salam Aleykum to my first Maven Project from Servlet");
        String passw = "123";
        String qname = request.getParameter("name");
        String password = request.getParameter("pwd");
        if(password.equals(passw)){
            request.setAttribute("name",qname);
            RequestDispatcher rq1 = request.getRequestDispatcher("welcome.jsp");
            rq1.forward(request, response);
        }else{
            printWriter.println("Sorry username or password invaid!");
            RequestDispatcher rq = request.getRequestDispatcher("passerror.jsp");
            rq.forward(request,response);
        }


        }
}
